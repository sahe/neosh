# Was ist NeoSH?
Eine Neuentwicklung eines AutoHotkey "Treibers" für Windows für das Tastaturlayout NEO. 
Mit AutoHotkey wird für jede Taste ein Shortcut erstellt und ein anderes Zeichen endsprechend dem NEO-Layout weitergesendet. Windows muss für eine korrekte Funktionsweise auf das deutsche QWERTZ-Layout verwenden. Die portable exe Datei die von AutoHotkey erstellt wurde kann ohne Installation und ohne Admin-Rechte gestartet werden. Im Systray von Windows erscheint ein AutoHotkey-Icon in dessen Kontext-Menü das NEO-Layout deaktiviert werden kann.

# Mehr Informationen über Neo?
    -> https://de.wikipedia.org/wiki/Neo_(Tastaturbelegung)
	-> https://neo-layout.org/
    
# Features
## 1. Kill-Shift
Wenn eine Taste schnell (<200ms) gedrückt wird, erscheint der kleine Buchstabe.
Wird die Taste lange gedrückt (200ms bis 500ms), erscheint der große Buchstabe.

## 2. Layer 1+2
![Layer 1 und 2](https://gitlab.com/sahe/neosh/raw/master/docs/Ebene%201+2.png)

## 3. Layer 3
![Layer 3](https://gitlab.com/sahe/neosh/raw/master/docs/Ebene%203.png)

## 4. Layer 4
![Layer 4](https://gitlab.com/sahe/neosh/raw/master/docs/Ebene%204.png)

## Layer 5 und 6
Layer 5 und 6 sind leider nicht implementiert.

# Versionen
## NeoSH.exe
NEO Layout mit Ebene 3 und 4 + Kill-Shift

## QwertzLongPressLayer3+4.exe
QWERTZ Layout mit Ebene 3 und 4 + Kill-Shift

## QwertzLongPress.exe
QWERTZ Layout ohne Ebene 3 und 4, aber mit dem Kill-Shift Feature


