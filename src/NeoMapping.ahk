; LongPressSubstitude3(	originKey, 
;						lowerKey, 
;						upperKey, 					# if empty (""), longpress is not executed
;						layer3key := "", 			# if empty, no char is sent
;						layer4key := "", 			# if empty, no char is sent
;						layer34key := "", 			# if empty, no char is sent
;						longPressTimeout := 2)		# 1 = little time to detect long-press, 2 = medium time, 3 = long time

; links oben
*q::LongPressSubstitude3("q", "x", "X", "", "", "", 3)
*w::LongPressSubstitude3("w", "v", "V", "_", "{Backspace}", "{Ctrl down}{Shift down}{Left}{Shift up}{Ctrl up}")
*e::LongPressSubstitude3("e", "l", "L", "[", "{Up}", "{Shift down}{Up}{Shift up}")
*r::LongPressSubstitude3("r", "c", "C", "]", "{Delete}", "{Ctrl down}{Shift down}{Right}{Shift up}{Ctrl up}")
*t::LongPressSubstitude3("t", "w", "W", "t", "{PgUp}", "")

; links home row
*a::LongPressSubstitude3("a", "u", "U", "\", "{Home}", "{Shift down}{Home}{Shift up}", 3)
*s::LongPressSubstitude3("s", "i", "I", "/", "{Left}", "{Shift down}{Left}{Shift up}")
*d::LongPressSubstitude3("d", "a", "A", "{U+007B}", "{Down}", "{Shift down}{Down}{Shift up}")		; {
*f::LongPressSubstitude3("f", "e", "E", "{U+007D}", "{Right}", "{Shift down}{Right}{Shift up}") 	; }
*g::LongPressSubstitude3("g", "o", "O", "*", "{End}", "{Shift down}{End}{Shift up}")

; links unten
*SC02C::LongPressSubstitude3("y", Chr(252), Chr(220), "{U+0023}", "", "", 3) 						;y;ü;Ü;#
*x::LongPressSubstitude3("x", Chr(246), Chr(214), "$", "{Tab}", "{Ctrl down}x{Ctrl up}")  								;ö;Ö	
*c::LongPressSubstitude3("c", Chr(228), Chr(196), "|", "{ESC}", "{Ctrl down}c{Ctrl up}")  								;ä;Ä
*v::LongPressSubstitude3("v", "p", "P", "~", "{Enter}", "{Ctrl down}v{Ctrl up}")
*b::LongPressSubstitude3("b", "z", "Z", "", "{PgDn}", "{Ctrl down}z{Ctrl up}")

; rechts oben
*z::LongPressSubstitude3("z", "k", "K", "{U+0021}", "", "") 										;!
*u::LongPressSubstitude3("u", "h", "H", "<", "7", "{Ctrl down}h{Ctrl up}")
*i::LongPressSubstitude3("i", "g", "G", ">", "8", "")
*SC018::LongPressSubstitude3("o", "f", "F", "{U+003D}", "9", "{Ctrl down}f{Ctrl up}") 				;=
*p::LongPressSubstitude3("p", "q", "Q", "&", "", "", 3)
*SC01A::SC00C 																						;ü::ß
;	SC01A up::LongPressSubstitude(1, Chr(223))														;ß

; rechts home row
*h::LongPressSubstitude3("h", "s", "S", "?", "", "{Ctrl down}s{Ctrl up}")
*j::LongPressSubstitude3("j", "n", "N", "(", "4", "{Ctrl down}n{Ctrl up}")
*k::LongPressSubstitude3("k", "r", "R", ")", "5", "")
*l::LongPressSubstitude3("l", "t", "T", "-", "6", "{Ctrl down}t{Ctrl up}")
*SC027::LongPressSubstitude3("SC027", "d", "D", ":", "", "")
*SC028::LongPressSubstitude3("SC028", "y", "Y", "@", "", "", 3)

; rechts unten
*n::LongPressSubstitude3("n", "b", "B", "{U+002B}", "", "")											;+
*m::LongPressSubstitude3("m", "m", "M", "{U+0025}", "1", "{Ctrl down}{Left}{Ctrl up}")				;%   
*SC033::LongPressSubstitude3(",", ",", ";", """", "2", "{Ctrl down}{Right}{Ctrl up}")
*SC034::LongPressSubstitude3(".", ".", ":", "'", "3", "")
*-::LongPressSubstitude3("-", "j", "J", "", "", "", 3)

*Space::LongPressSubstitude3(" ", " ", "", " ", "0", "")