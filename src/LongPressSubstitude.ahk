LAlt:: 
	;disable ALT funktionality
return

; Map < to Alt in order to enable stuff like multiline editing
<::LAlt

LongPressSubstitude3(originKey, lowerKey, upperKey, layer3key := "", layer4key := "", layer34key := "", longPressTimeout := 2)
{
	Critical On 	; Do not interrupt the following code in order to preserve the order of key strokes.

	layer3 := GetKeyState("LAlt", "P")
	layer4 := GetKeyState("LControl", "P") & GetKeyState("RAlt", "P")

	If (layer3 & layer4)
	{
		IfNotEqual layer34key, ""
		{
			Send %layer34key%
		}
		
		return
	}
	
	If (layer4)
	{
		IfNotEqual layer4key, ""
		{
			Send %layer4key%
		}

		return
	}
	
	If (layer3)
	{
		IfNotEqual layer3key, ""
		{
			Send %layer3key%
		}

		return
	}
	
	If (GetKeyState("Ctrl", "P") && GetKeyState("Shift", "P"))
	{
		Send ^+%lowerKey%
		return
	}	

	If (GetKeyState("Ctrl", "P"))
	{
		Send ^%lowerKey%
		return
	}
	
	If (GetKeyState("Shift", "P"))
	{
		Send %upperKey% ;+%lowerKey
		return
	}
	
	If (GetKeyState("LWin", "P"))
	{
		Send #%lowerKey%
		return
	}	
	
	If (GetKeyState("<", "P")) ; Substitude for ALT, defined in Layer3
	{
		Send !%lowerKey%
		return
	}	

	; -> Die Foglende Abbruchbedingung scheint zwar sinnvoll, führt aber häufig zur verkehrte Abarbeitung der 
	; -> Tasktenanschläge (speziell im Zusammenhang mit der Space Taste, die einzig von dieser Bedingeng betroffen ist)
	; Do not check for longpres, if no upperKey is given
	; IfEqual upperKey,
	; {
	; 	Send %lowerKey%
	; 	return
	; }
		
	; wait until key is released or time is elapsed	
	If (longPressTimeout = 1)
		KeyWait, %originKey%, T0.170
	else If (longPressTimeout = 2)
		KeyWait, %originKey%, T0.220
	else
		KeyWait, %originKey%, T0.250
		
	IsLongPress := GetKeyState(originKey, "P")	; check if key is still pressed. If yes, longpress is detected
	
	IF (IsLongPress)
	{
		Send %upperKey%
		Thread, Priority,1 		;discard any further hotkeys with default prio 0
		Critical Off
		KeyWait, %originKey% 	; prevent windows from repeatedly sending the same keystroke. Also, dass nicht mehrerer Großbuchstaben hintereinander geschrieben werden.
	}
	else
	{
		Send %lowerKey%
	}
}